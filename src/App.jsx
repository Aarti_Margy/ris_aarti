import Heading from './Heading';
import Details from './Details';
import Contact from './Contact';
import Course from './Course';
import Qualification from './Qualification';
import Image from './Image';
import './App.css';

function App() {
  return (
    <div className="app">
      <Heading/>
      <Details/>
      <Contact/>
      <Course/>
      <Qualification/>
      <Image/>
    </div>
  );
}

export default App;
